<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Traints\UsesUuid;

class User extends Authenticatable
{
    use UsesUuid;
    protected $fillable = [ 'username','email','name','password', 'role_id','photo_profile'];
    protected $primaryKey = 'id';
    
    public function role()
    {
        return $this->belongsTo(Role::class,'id');
    }
}
