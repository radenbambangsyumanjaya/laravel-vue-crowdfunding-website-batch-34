<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Traints\UsesUuid;

class Role extends Model
{   use UsesUuid;
    protected $fillable = ['name'];
    protected $primaryKey = 'id';
    

    public function user(){
        return $this->hasMany(User::class,'role_id');
    }


    
}
