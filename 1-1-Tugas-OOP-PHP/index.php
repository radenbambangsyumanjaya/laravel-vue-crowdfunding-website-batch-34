<?php

trait Hewan{
    public $nama;
    public $darah = 50;
    public $jumlahKaki;
    public $keahlian;
    public function atraksi(){
        echo "{$this->nama} sedang {$this->keahlian}";
    }
}
abstract class Fight{
    use Hewan;
    public $attackPower;
    public $defencePower;

    public function serang($hewan){
        echo "{$this->nama} sedang menyerang {$hewan->nama} <br>";
        $hewan->diserang($this);
    }
    public function diserang($hewan){
        echo "{$this->nama} sedang diserang <br>";
        $this->darah = $this->darah -$hewan->attackPower/$this->defencePower;
    }

    protected function getInfo(){
        echo "<br>";
        echo "Nama: {$this->nama}";
        echo "<br>";
        echo "Jumlah Kaki: {$this->jumlahKaki}";
        echo "<br>";
        echo "Keahlian: {$this->keahlian}";
        echo "<br>";
        echo "Darah: {$this->darah}";
        echo "<br>";
        echo "Attack Power: {$this->attackPower}";
        echo "<br>";
        echo "Defence Power: {$this->defencePower}";
        echo "<br>";
        $this->atraksi();
    }
    abstract public function getInfoHewan();
}

class Elang extends Fight{
    public function __construct($nama){
        $this->nama = $nama;
        $this->jumlahKaki = 2;
        $this->keahlian = "terbang tinggi";
        $this->attackPower = 10;
        $this->defencePower= 8;
    }

    public function getInfoHewan(){
        echo "Jenis Hewan: Elang";
        $this->getInfo();
    }
}
class Harimau extends Fight{
    public function __construct($nama){
        $this->nama = $nama;
        $this->jumlahKaki = 4;
        $this->keahlian = "lari cepat";
        $this->attackPower = 7;
        $this->defencePower= 8;
    }

    public function getInfoHewan(){
        echo "Jenis Hewan: Harimau";
        $this->getInfo();
    }
}

class Baris{
    public static function buatBaris(){
        echo "<br>";
        echo "===========";
        echo "<br>";
    }
}

$elang_3 = new Elang("elang_3");
$elang_3->getInfoHewan();

Baris::buatBaris();

$harimau_1 = new Harimau("harimau_1");
$harimau_1->getInfoHewan();

Baris::buatBaris();

$harimau_1->serang($elang_3);

Baris::buatBaris();

$elang_3->getInfoHewan();

Baris::buatBaris();

$harimau_1->getInfoHewan();


?>